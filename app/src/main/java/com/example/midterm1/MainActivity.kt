package com.example.midterm1

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.password
import kotlinx.android.synthetic.main.activity_main.username
import kotlinx.android.synthetic.main.forgot_password.*
import java.util.concurrent.TimeUnit
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    public override fun onStart() {

        super.onStart()

        val currentUser = auth.currentUser
        update(currentUser)
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        sign_up.setOnClickListener {

            startActivity(Intent(this, RegisterActivity::class.java))
            finish()

        }

        log_in.setOnClickListener {

            if (validateMail(username) && validatePass(password)) {
                logIn(username.text.toString(), password.text.toString())

            }
        }

        forgot_password.setOnClickListener {
            changePass()

        }
    }

    private fun logIn(ml : String, pass : String) {
        auth.signInWithEmailAndPassword(ml, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    update(user)
                } else {
                    update(null)
                }
            }
    }
    fun validatePass(customID : EditText) : Boolean{

        if (customID.text.toString().isEmpty() || customID.text.toString().length <= 4) {
            customID.error = "Please Enter A Password In Correct Format That Is Longer Than 4 Characters"
            customID.requestFocus()

            return false

        }

        return true

    }

    private fun changePass() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Forgot Password")
        val view = layoutInflater.inflate(R.layout.forgot_password, null)
        val resetMail = view.findViewById<EditText>(R.id.f_pass_username)

        builder.setView(view)
        builder.setPositiveButton("Reset", DialogInterface.OnClickListener { _, _ ->
            if (validateMail(resetMail)) {
                resetPass(resetMail.text.toString())
            }

            else
            {
                changePass()
                Toast.makeText(this, "Input In The Right Format", Toast.LENGTH_SHORT).show()
            }

        })

        builder.setNegativeButton("Close", DialogInterface.OnClickListener { _, _ ->})
        builder.show()

    }

    private fun resetPass(email : String) {

        Log.d("myTag", "resetPass")
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Email Has Been Sent", Toast.LENGTH_SHORT).show()

                }
            }


    }


    fun validateMail(customID : EditText) : Boolean{

        if (customID.text.toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(customID.text.toString()).matches()) {
            customID.error = "Please Enter A Valid Email Address"
            customID.requestFocus()

            return false
        }

        return true

    }

    private fun update(currentUser : FirebaseUser?) {

        if (currentUser != null) {
            startActivity(Intent(this, HomeActivity :: class.java))

        }

        else {

            Toast.makeText(baseContext, "Logged In", Toast.LENGTH_LONG).show()
        }

    }

}