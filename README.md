Registry Entry - არის აპლიკაცია სადაც შეგიძლია ნახო შენი რომელი თანამშრომლები არიან სამსახურში.
ინფორმაცია თანამშრომლების სამსახურში ყოფნასთან დაკავშირებით მოაქვს SmartCard-ებიდან,
რომელსაც თანამშრომელი ატარებს ოფისის შესასვლელში როდესაც ის შედის სამსახურში.
ეს ინფორმაცია შეიძლება გჭირდებოდეს რათა ნახო რომელი შენი თანამშრომლები არიან სამსახურში ისე, რომ
მათ არ მიწეროთ დაურეკოთ, ან რამდენიმე სართული გაიაროთ რათა უბრალოდ ნახოთ მათი ადგილმდებარეობა.

აპლიკაციის სრულფასოვანი მუშაობისთვის როდესაც ბილდი და ინსტალაცია დასრულდება ვირტუალურ დივაისზე, შეწყვიტეთ რუნი, აპლიკაცია ხელახლა გახსენით და შეასრულეთ რისი შესრულებაც გინდოდათ.

აპლიკაცია შედგება 3 გვერდისგან
მთავარი გვერდი, ანუ Log In ფეიჯი სადაც მომხარებელი ამჯერად თანამშრომელი გადის ავტორიზაციას.
ამ გვერდზე მას აქვს საშუალება, როგორც უკვე ვახსენე ავტორიზაციის გავლის, რეგისტრაციის, ან პაროლის აღდგენის.

რეგისტრაციის გასასვლელად საჭიროა Register ღილაკზე დაჭერით გადახვიდეთ შესაბამის გვერდზე.
რეგისტრაციის შემთხვევაში თანამშრომელი აუცილებელია დარეგისტრირდეს სამსახურის მეილით და შეიყვანოს სასურველი პაროლი ორჯერ.

პაროლის აღსადგენად შესაბამის ღილაკზე დაჭერით და მეილის მითითებით გამოიგზავნება თქვენს ელფოსტაზე პაროლის აღდგენის ბმული.

ავტორიზაციის გავლის შემდეგ მომხარებელი გადადის Home ფეიჯზე სადაც მას აქვს საშუალება ნახოს ჩამონათვალი ყველა იმ მომხმარებლისა ვინც არის ოფისში.